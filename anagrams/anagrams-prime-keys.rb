require 'prime'

class AnagramsPrime
  attr_reader :dictionary

  def initialize(words)
    @dictionary = words.group_by do |word|
      prime_value(word)
    end
  end

  def find(letters)
    letters_value = prime_value(letters)
    dictionary.select {|k, _| letters_value % k == 0}.values.flatten
  end

  private

  def letter_values
    @letter_values ||= Hash[('a'..'z').zip(Prime.each)]
  end

  def prime_value(word)
    word.each_char.inject(1) {|acc, val| acc *= letter_values[val]}
  end
end
