# Determine if two words are anagrams of each other.
#
# == Parameters:
# entries::
#   An `Enumerable` of strings.
#
# == Returns:
# True iff all the members of `entries` are anagrams of each other.
def anagrams?(*entries)
  entries.group_by {|entry| equivalence_class(entry)}.count == 1
end

def equivalence_class(word)
  word.each_char.sort
end

class Anagrams
  attr_reader :dictionary

  # Anagram solver constructor
  #
  # == Parameters:
  # words::
  #   An `Enumerable` of strings. For the purpose of English-language
  #   anagrams, assume all characters in each string are lowercase letters.
  def initialize(words)
    @dictionary = words.group_by {|word| equivalence_class(word)}
  end

  # Find all the words in the dictionary that can be made with all of
  # the given letters.
  #
  # == Parameters:
  # letters::
  #   An `Enumerable` of single-character strings.
  #
  # == Returns:
  #   An array of words.
  def find(letters)
    dictionary.fetch(equivalence_class(letters))
  end
end
