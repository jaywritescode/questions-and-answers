class Anagrams
  attr_reader :dictionary

  # Anagram solver constructor
  #
  # == Parameters:
  # words::
  #   An `Enumerable` of strings. Assume all the characters in each string are
  #   lowercase letters.
  def initialize(words)
    @dictionary = words.group_by do |word|
      LetterCounter.new(word)
    end
  end

  # Find all the words that can be made with the given collection of letters.
  #
  # == Parameters:
  # letters::
  #   An `Enumerable` of single-character strings.
  #
  # == Returns:
  # A list of the dictionary words that can be made from the given letters.
  def find(letters)
    letter_counter = LetterCounter.new(letters)
    dictionary.select {|k, _| k.wildcard_subset?(letter_counter)}.values.flatten
  end

  private

  class LetterCounter
    @@WILCARD = '*'

    # Constructor
    #
    # == Parameters:
    # word::
    #   A string
    def initialize(word)
      word.each_char.each_with_object(tally) do |char, _|
        increment(char)
      end
    end

    # Determine if this counter (i.e. mulitset) is a subset of another counter, with wildcards.
    #
    # == Parameters:
    # other_counter::
    #   The other counter
    def wildcard_subset?(other_counter)
      num_wildcards = other_counter.lookup(@@WILCARD)
      misses = 0

      tally.keys.each do |key|
        misses += [0, lookup(key) - other_counter.lookup(key)].max
        return false if misses > num_wildcards
      end

      true
    end

    # Find the number of times `key` has been counted.
    #
    # == Parameters:
    # key::
    #   The string (single character) we're looking up.
    #
    # == Returns:
    # The number of times `key` has been counted.
    def lookup(key)
      tally.fetch(key, 0)
    end

    def ==(other)
      tally == other.tally
    end

    def eql?(other)
      self == other
    end

    protected

    def tally
      @tally ||= Hash.new
    end

    private

    def increment(key)
      tally[key] = 0 unless tally.has_key?(key)
      tally[key] += 1
    end
  end
end


anagrams = Anagrams.new(%w(pass past last lost loss))
p anagrams.find("poss*")
p anagrams.find("pos**")
