Anagrams
========

Given a list of words and a collection of letters, find all the words in the list that can be made with some or all of the letters.

Furthermore, assume that this calculation will be run many times with different inputs against the same list of words. That is, feel free to pre-process the list of words.

Variants
========

+ `anagrams-simple` searches for strict anagrams. Two words are strict anagrams iff their letters, sorted in alphabetical order, are the same.

+ `anagrams` and `anagrams-prime-keys` search for loose anagrams. Two words
are loose anagrams if the letters in one are a (possibly improper) sub-collection of the other.

+ `anagrams-with-wildcards` searches for loose anagrams where an asterisk character can match any letter.

Benchmarking sample (times in seconds)
--------------------------------------

|274,926 words, 50 trial searches|user |system  |total|real        |
|--------------------------------|-----|--------|-----|------------|
|LetterCounter: preprocessing    | 8.09|0.34    | 8.43|(  9.180727)|
|LetterCounter: searching        |28.72|0.58    |29.30|( 31.520108)|
|AnagramsPrime: preprocessing    | 1.92|0.04    | 1.96|(  2.066391)|
|AnagramsPrime: searching        | 5.36|0.15    | 5.51|(  6.014490)|

---

Words from https://github.com/atebits/Words/blob/master/Words/en.txt
