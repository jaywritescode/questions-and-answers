# Determine if two or more words or phrases are anagrams of each other.
#
# == Parameters:
# entries::
#   An `Enumerable` of strings.
#
# == Returns:
# True iff all the members of `entries` are anagrams of each other.
def anagrams?(*entries)
  entries.group_by do |word|
    word.downcase.each_char.select {|char| ('a'..'z').include? char}.sort
  end.count == 1
end

class Anagrams
  attr_reader :dictionary

  # Anagram solver constructor
  #
  # == Parameters:
  # words::
  #   An `Enumerable` of strings. Assume all the characters in each string are
  #   lowercase letters.
  def initialize(words)
    @dictionary = words.group_by do |word|
      LetterCounter.new(word)
    end
  end

  # Find all the words that can be made with the given collection of letters.
  #
  # == Parameters:
  # letters::
  #   An `Enumerable` of single-character strings.
  #
  # == Returns:
  # A list of the dictionary words that can be made from the given letters.
  def find(letters)
    letter_counter = LetterCounter.new(letters)
    dictionary.select {|k, _| k.subset?(letter_counter)}.values.flatten
  end

  private

  class LetterCounter
    # Constructor.
    #
    # == Parameters:
    # word::
    #   A string.
    def initialize(word)
      word.each_char.each_with_object(tally) do |char, _|
        increment(char)
      end
    end

    # Determine if this counter (i.e. multiset) is a subset of another counter.
    #
    # == Parameters:
    # other_counter::
    #   The other counter.
    def subset?(other_counter)
      tally.keys.all? do |k|
        tally[k] <= other_counter.lookup(k)
      end
    end
    alias_method :<=, :subset?

    # Find the number of times `key` has been counted.
    #
    # == Parameters:
    # key::
    #   The string (single character) we're looking up.
    #
    # == Returns:
    # The number of times `key` has been counted.
    def lookup(key)
      tally.fetch(key, 0)
    end

    def ==(other)
      tally == other.tally
    end

    def eql?(other)
      self == other
    end

    protected

    def tally
      @tally ||= Hash.new
    end

    private

    def increment(key)
      tally[key] = 0 unless tally.has_key?(key)
      tally[key] += 1
    end
  end
end
