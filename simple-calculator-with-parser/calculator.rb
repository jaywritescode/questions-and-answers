require 'minitest/autorun'
require 'byebug'

class SimpleCalculator
  @@PRECEDENCE = {
    :+ => 3,
    :- => 3,
    :* => 2,
    :/ => 2,
    :** => 1,
    nil => Float::INFINITY
  }

  @@OPEN_PAREN = :'('
  @@CLOSE_PAREN = :')'

  def evaluate(str)
    tokens = str.split.map do |token|
      token =~ /\d+/ ? token.to_i : token.to_sym
    end
    _evaluate(tokens).first
  end

  Node = Struct.new(:value, :left_child, :right_child)

  private

  def get_expression(tokens, initial = 0)
    case
    when tokens[initial] == @@OPEN_PAREN
      get_parenthesized_expression(tokens, initial)
    when is_number?(tokens[initial])
      tokens.slice(initial, 1)
    end
  end

  def get_parenthesized_expression(tokens, initial = 0)
    # byebug

    return nil unless tokens[initial] == @@OPEN_PAREN

    counter = 0
    tokens.drop(initial).each_with_index do |v, i|
      counter += 1 if v == @@OPEN_PAREN
      counter -= 1 if v == @@CLOSE_PAREN
      return tokens.slice(initial, i + 1) if counter == 0
    end
  end

  def _evaluate(tokens)
    loop do
      return tokens if tokens.count == 1

      # byebug

      left_expr = get_expression(tokens, 0)
      left = _evaluate(if left_expr[0] == @@OPEN_PAREN then left_expr.slice(1, left_expr.count - 2) else left_expr end)
      tokens = left + tokens[left_expr.count .. tokens.count]

      # since we reduced the left expression to a single term, we know that
      # tokens[0] is a number and tokens[1] is an operator
      operator_index = 1

      # also, we know the right-hand expression for the operator starts at
      # tokens[2]
      right_expr = get_expression(tokens, 2)
      right = _evaluate(if right_expr[0] == @@OPEN_PAREN then right_expr.slice(1, right_expr.count - 2) else right_expr end)
      tokens = tokens[0 ... 2] + right + tokens[2 + right_expr.count .. tokens.count]

      # the right-hand expression has been reduced to a single token, so we know
      # that tokens[operator_index + 2] is an operator
      next_operator_index = operator_index + 2
      loop do
        break if precedence(tokens[operator_index]) <= precedence(tokens[next_operator_index])
        operator_index += 2
        next_operator_index += 2
      end

      left, operator, right = tokens[operator_index - 1 .. operator_index + 1]
      tokens = (tokens[0 ... operator_index - 1] << left.send(operator, right)) + tokens[next_operator_index .. tokens.count]
    end
  end

  def precedence(operator)
    @@PRECEDENCE[operator]
  end

  def is_number?(n)
    n.is_a? Numeric
  end

  def is_operator?(str)
    [:+, :-, :*, :/, :**].include? str
  end
end

describe SimpleCalculator do
  before do
    @calc = SimpleCalculator.new
  end

  describe 'math' do
    it 'does simple addition' do
      @calc.evaluate('4 + 5').must_equal 9
    end

    it 'performs multiple operations' do
      @calc.evaluate('2 * 3 * 4').must_equal 24
    end

    it 'performs operations in the right order' do
      @calc.evaluate('37 - 4 * 2 ** 3 + 7').must_equal 12
    end

    it 'performs operations in the right order' do
      @calc.evaluate('4 * 5 - 3 ** 2 + 8').must_equal 19
    end

    it 'handles parentheses' do
      @calc.evaluate('( 4 + 1 ) * 3').must_equal 15
    end

    it 'handles parentheses' do
      @calc.evaluate('4 * ( 1 + 2 )').must_equal 12
    end
  end
end
