def permutations(collection, k)
  return [[]] if k == 0

  collection.each_index.inject([]) do |acc, index|
    acc + permutations(collection.slice(0, index) + collection.slice(index + 1, collection.count), k - 1).collect do |sublist|
      [collection[index]] + sublist
    end
  end
end

class Permutations
  def initialize(collection, k)
    @collection = collection
    @k = k
  end

  def enumerator
    Enumerator.new do |y|
      y.yield [] if @k == 0

      @collection.each_index do |index|
        p = Permutations.new(@collection.slice(0, index) + @collection.slice(index + 1, @collection.count), @k - 1)
        p.enumerator.each do |sublist|
          y.yield [@collection[index]] + sublist
        end
      end
    end
  end
end

puts permutations(%w(a b c d e), 2).to_s

Permutations.new(%w(a b c d e), 4).enumerator.each do |p|
  puts p.join
end
