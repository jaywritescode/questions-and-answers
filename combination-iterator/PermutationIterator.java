import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import static java.util.stream.Collectors.*;

public class PermutationIterator<T> implements Iterator<List<T>> {

    private final ArrayList<T> c;
    private final int k;
    private final int n;

    private final boolean[] alreadyUsed;
    private final int[] counter;
    private boolean done = false;

    PermutationIterator(Collection<T> collection, int k) {
        this.c = new ArrayList<>(collection);
        this.k = k;
        this.n = this.c.size();

        Arrays.setAll(this.counter = new int[k], IntUnaryOperator.identity());
        
        this.alreadyUsed = new boolean[n];
        IntStream.range(0, k).forEach(i -> this.alreadyUsed[i] = true);
    }

    public boolean hasNext() {
        return !done;
    }

    public List<T> next() {
        if (done) {
            return null;
        }

        List<T> p = IntStream.of(counter).mapToObj(idx -> c.get(idx)).collect(toList());

        increment();

        return p;
    }

    private void increment() {
        if (k == 0) {
            done = true;
            return;
        }

        int head = counter.length;
        OptionalInt nextValue = OptionalInt.empty();

        while (!nextValue.isPresent()) {
            --head;

            if (head < 0) {
                done = true;
                return;
            }

            alreadyUsed[counter[head]] = false;
            nextValue = IntStream.range(counter[head] + 1, n)
                .filter(idx -> !this.alreadyUsed[idx])
                .findFirst();
        }

        counter[head] = nextValue.getAsInt();
        alreadyUsed[counter[head]] = true;

        while (++head < counter.length) {
            counter[head] = IntStream.range(0, n)
                .filter(idx -> !this.alreadyUsed[idx])
                .findFirst()
                .getAsInt();
            alreadyUsed[counter[head]] = true;
        }
    }

    public static void main(String... args) {
        PermutationIterator<String> iter;

        List<String> items = Arrays.asList("antelope", "buzzard", "chinchilla", "dugong", "elk");
 
        for (int i = 0; i <= items.size(); ++i) {
            iter = new PermutationIterator<>(items, i);
            while (iter.hasNext()) {
                System.out.println(iter.next());
            }
            System.out.println();
        }
    }
}