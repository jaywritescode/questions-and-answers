import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import static java.util.stream.Collectors.*;

public class CombinationIterator<T> implements Iterator<List<T>> {

    private final ArrayList<T> c;
    private final int k;
    private final int n;

    private final int[] counter;
    private boolean done = false;

    CombinationIterator(Collection<T> collection, int k) {
        this.c = new ArrayList<>(collection);
        this.k = k;
        this.n = this.c.size();

        Arrays.setAll(this.counter = new int[k], IntUnaryOperator.identity());
    }

    public boolean hasNext() {
        return !done;
    }

    public List<T> next() {
        if (done) {
            return null;
        }

        List<T> p = IntStream.of(counter).mapToObj(idx -> c.get(idx)).collect(toList());

        increment();

        return p;
    }

    private void increment() {
        if (k == 0) {
            done = true;
            return;
        }

        int head = counter.length - 1,
            ref = counter[head];
        while (ref + 1 > n - (k - head)) {
            --head;
            if (head < 0) {
                done = true;
                return;
            }
            ref = counter[head];
        }
        counter[head]++;
        while (++head < counter.length) {
            counter[head] = counter[head - 1] + 1;
        }
    }

    public static void main(String... args) {
        CombinationIterator<String> iter;

        List<String> items = Arrays.asList("antelope", "buzzard", "chinchilla", "dugong", "elk");

        for (int i = 0; i <= items.size(); ++i) {
            iter = new CombinationIterator<>(items, i);
            while (iter.hasNext()) {
                System.out.println(iter.next());
            }
            System.out.println();
        }
    }
}
