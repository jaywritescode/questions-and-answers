# returns an array of k-combinations over a given collection
def combinations(collection, k)
  return [[]] if k == 0

  collection.each_index.inject([]) do |acc, index|
    acc + combinations(collection.slice(index + 1, collection.count), k - 1).collect do |sublist|
      [collection[index]] + sublist
    end
  end
end

class Combinations
  def initialize(collection, k)
    @collection = collection
    @k = k
  end

  # returns a lazy enumerator of @k-combinations over @collection
  def enumerator
    Enumerator.new do |y|
      y.yield [] if @k == 0
      
      @collection.each_index do |index|
        c = Combinations.new(@collection.slice(index + 1, @collection.count), @k - 1)
        c.enumerator.each do |sublist|
          y.yield [@collection[index]] + sublist
        end
      end
    end
  end
end


puts combinations(%w(a b c d e), 2).to_s

Combinations.new(%w(a b c d e), 4).enumerator.each do |c|
  puts c.join
end
