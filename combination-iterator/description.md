### Combination iterator

Implement Python's `itertools.combinations` in some other language.

Hint: Use recursion! Especially for the permutation iterator.

Hint for the generator-iterator: Store the "done" state in a separate variable. Don't try and calculate it on the fly in the `hasNext()` method.