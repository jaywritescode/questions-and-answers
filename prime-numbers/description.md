### Prime number questions

Someone asked to write a function that takes a number `k` and returns all the prime numbers less than (or less than or equal to) `k`. I was cocky and made some rookie dumb-dumb choices and got the question wrong, so this is my penance.

#### Iteration 1.0

```python
def is_prime(k):
	for n in range(2, k):
		if k % n == 0:
            return False
    return True

def find_primes(k):
    primes = []
    for n in range(2, k):
        if is_prime(n):
            primes.append(n)
    return primes
```

#### Iteration 1.1

```python
def is_prime(k):
    for n in range(2, k):
        if k % n == 0:
            return False
        if n * n > k:
            break
    return True

def find_primes(k):
    primes = [2]            # assume k > 2
    for n in range(3, k, 2):
        if is_prime(n):
            primes.append(n)
    return primes
```

#### Iteration 2.0

Keep the list of prime numbers in scope, then we only have to see if `k` is divisible by any of the primes.

```python
def find_primes(k):
    primes = []
    for n in range(2, k):
        if not any(n % p == 0 for p in primes):
            primes.append(n)
    return primes
```

#### Iteration 2.1

Use `takewhile` to end the loop once we hit the square root of `k`.

```python
def find_primes(k):
    primes = []
    for n in range(2, k):
        sublist = itertools.takewhile(lambda y: y * y <= n, primes)
        if not any(n % p == 0 for p in sublist):
            primes.append(n)
    return primes
```

#### Iteration 2.2


```python
def find_primes(k):
    primes = []
    for n in range(2, k):
        if not any(n % p == 0 for p in itertools.takewhile(lambda y: y * y <= n, primes)):
            primes.append(n)
    return primes
```

Don't use the Sieve of Erathosthenes -- it's too difficult to derive the setup. `sieve = {n: True for n in range(2, k)}` maybe?