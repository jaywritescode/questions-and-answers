var _ = require('lodash');

function findPrimes(k) {
    var primes = []
    _.range(2, k).forEach((n) => {
        if (!_(primes).takeWhile(y => y * y <= n).some(p => n % p == 0)) {
            primes.push(n);
        }
    });
    return primes;
}

console.log(findPrimes(process.argv[2]));