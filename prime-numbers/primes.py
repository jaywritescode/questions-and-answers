import itertools

def find_primes(k):
    """
    Find all primes less than k
    """
    primes = []
    for n in range(2, k):
        if not any(n % p == 0 for p in itertools.takewhile(lambda y: y * y <= n, primes)):
            primes.append(n)
    return primes