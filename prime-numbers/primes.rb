def find_primes(k)
  primes = []
  (2..k).each do |n|
    primes << n unless primes.take_while {|y| y * y <= n}.any? {|p| n % p == 0}
  end
  primes
end
