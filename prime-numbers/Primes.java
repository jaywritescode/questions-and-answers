import java.util.ArrayList;
import java.util.stream.IntStream;

public class Primes {

    public static ArrayList<Integer> findPrimes(int k) {
        ArrayList<Integer> primes = new ArrayList<>();
        IntStream.range(2, k).forEach(n -> {
            if (primes.stream().noneMatch(p -> n % p == 0)) {
                primes.add(n);
            }
        });
        return primes;
    }

    public static void main(String... args) {
        System.out.println(findPrimes(Integer.parseInt(args[0])));
    }
}