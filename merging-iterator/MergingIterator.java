import java.util.*;

public class MergingIterator<T> implements Iterator<T> {

    private final Set<Iterator<T>> iterators;
    private final PriorityQueue<T> heap;
    private final Map<T, Iterator<T>> valueToIteratorMap;
  
    MergingIterator(Collection<Iterator<T>> iterators, Comparator<T> comparator) {
        this.iterators = new HashSet<>(iterators);

        heap = new PriorityQueue<>(iterators.size(), comparator);
        valueToIteratorMap = new HashMap<>(iterators.size());

        iterators.forEach(this::pollIterator);
    }

    @Override
    public boolean hasNext() {
        return !heap.isEmpty();
    }

    @Override
    public T next() {
        T value = heap.poll();
        pollIterator(valueToIteratorMap.get(value));
        return value;
    }

    private void pollIterator(Iterator<T> iterator) {
        T item;
        if (iterator.hasNext()) {
            heap.add(item = iterator.next());
            valueToIteratorMap.put(item, iterator);
        }
        else {
            iterators.remove(iterator);
        }
    }

    public static void main(String... args) {
        Collection<Iterator<String>> collection = Arrays.asList(
            Arrays.asList("AARdvark", "BEAR", "cat", "dingo", "elepHANT").iterator(),
            Arrays.asList("anteater", "buffalo", "CARnivorous ape", "dung BEETLE", "elk", "Flying Squirrel").iterator(),
            Arrays.asList("cheetah", "eagle").iterator(),
            Arrays.asList("ANT", "Antelope", "asp", "Bigfoot").iterator()
        );
        MergingIterator<String> iterator = new MergingIterator<>(collection, String.CASE_INSENSITIVE_ORDER);

        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
