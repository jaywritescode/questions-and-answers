### Merging iterator.

Problem: Implement a merging iterator that takes a collection of iterators along with a comparator function and merges their elements in the comparator's order. Assume each iterator in the collection is pre-sorted.

Here's a skeleton implementation in Java.

```java
public class MergingIterator<T> implements Iterator<T> {

    MergingIterator(Collection<Iterator<T>> iterators, Comparator<T> comparator) {
        // TODO
    }

    @Override
    public boolean hasNext() {
        // TODO
    }

    @Override
    public T next() {
        // TODO
    }
}
```

Notes:

+ in Java, we have the `PriorityQueue` object that implements a heap
+ in Python, there's `heapq`: [https://docs.python.org/3.5/library/heapq.html](https://docs.python.org/3.5/library/heapq.html)
+ in Javascript and Ruby, we need to write our own priority queue :(