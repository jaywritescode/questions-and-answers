### Directed graph paths

Let's say you're given a directed graph.

First, come up with a data structure to store this graph.

Then solve the following problem:

Given the start and end nodes and a value n, count the number of paths
whose length is at most n from the start node to the end node.

Solve it in Java, because maybe your interviewer is unfamiliar with Python.
