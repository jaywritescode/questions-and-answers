import java.util.*;

class DirectedGraph {

    Map<String, Node> nodes;

    private class Node {
        String label;
        Set<Edge> incoming, outgoing;

        Node(String label) {
            this.label = label;
            this.incoming = new HashSet<>();
            this.outgoing = new HashSet<>();
        }
    }

    private class Edge {
        Node start, end;
        int cost;

        Edge(Node start, Node end) {
            this(start, end, 1);
        }

        Edge(Node start, Node end, int cost) {
            this.start = start;
            this.end = end;
            this.cost = cost;
        }
    }

    public DirectedGraph() {
        this.nodes = new HashMap<>();
    }

    boolean addNode(String label) {
        if (nodes.containsKey(label)) {
            return false;
        }
        nodes.put(label, new Node(label));
        return true;
    }

    boolean addEdge(String startNode, String endNode) {
        return this.addEdge(nodes.get(startNode), nodes.get(endNode));
    }

    boolean addEdge(Node startNode, Node endNode) {
        if (startNode == null || endNode == null) {
            return false;
        }
        Edge e = new Edge(startNode, endNode);
        startNode.outgoing.add(e);
        endNode.incoming.add(e);
        return true;
    }

    class DFSNode<T> {
        T payload;
        int depth;

        DFSNode(T payload) {
            this(payload, 0);
        }

        DFSNode(T payload, int depth) {
            this.payload = payload;
            this.depth = depth;
        }
    }

    int countPaths(Node start, Node end, int maxSteps) {
        int count = 0;

        DFSNode<Node> _start = new DFSNode(start, 0), current, n;
        ArrayDeque<DFSNode<Node>> queue = new ArrayDeque() {{
            this.add(_start);
        }};

        while(!queue.isEmpty()) {
            current = queue.pop();
            if (current.payload == end) {
                ++count;
            }
            for (Edge outgoing : current.payload.outgoing) {
                n = new DFSNode<Node>(outgoing.end, current.depth + 1);
                if (n.depth < maxSteps) {
                    queue.add(n);
                }
            }
        }

        return count;
    }

    public static void main(String... args) {
        DirectedGraph g = new DirectedGraph();
        g.addNode("A");
        g.addNode("B");
        g.addNode("C");

        g.addEdge("A", "B");
        g.addEdge("B", "A");
        g.addEdge("B", "C");
        g.addEdge("C", "A");
        g.addEdge("C", "B");

        System.out.println(g.countPaths(g.nodes.get("A"), g.nodes.get("C"), 5));
    }
}