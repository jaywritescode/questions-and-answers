var _ = require('lodash');

class CoinMachine {
  constructor(coins) {
    this.coins = coins.sort((a, b) => b - a);
  }

  give_change(target, coins) {
    coins = coins || this.coins;
    if (coins.length == 0 && target > 0) {
      return null;
    }

    let c = coins[0],
        m = Math.floor(target / c),
        t = target % c,
        coin_count = (change) => Object.values(change).reduce((a, b) => a + b);
    if (t == 0) {
      return { [c]: m };
    }

    let p, q;
    if ((p = this.give_change(t, coins.slice(1))) == null) {
      return null;
    }
    else if ((q = this.give_change(target, coins.slice(1))) == null || coin_count(q) > coin_count(p) + m) {
      return Object.assign(p, { [c]: m });
    }
    else {
      return q;
    }
  }
}

let cm = new CoinMachine([10, 25, 1, 5]);
console.log(cm.give_change(73));

let cm2 = new CoinMachine([1, 4, 5, 10]);
console.log(cm2.give_change(8))