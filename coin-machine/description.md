Coin Machine
============

Given a coin machine with certain denominations of change, find the way to obtain a certain amount of money with the smallest possible number of coins.

Run with Babel:

```sh
npm install --global babel-cli
```

```sh
babel-node coin-machine.js
```

What's happening
----------------

Let's say we have a target amount `T` and coins `[C_0, C_1, C_2, ..., C_n]` where `i < j => C_i > C_j`.

A conjecture: If we have a solution with `k` `C_0` coins in it, then there is no way to find a better solution with `0 < k' < k` `C_0` coins in it.

So the solution either has zero `C_0` coins, or it doesn't.

### Case 1: The solution has at least on `C_0` coins.

In this case, we know the solution has `m = Math.floor(T / C_0)` `C_0` coins. There are `T % C_0` cents remaining. So we recursively call `give_change(T % C_0, [C_1, C_2, ..., C_n])` and `_.extend` its return value with `{[C_0]: m}`.

### Case 2: The solution has zero `C_0` coins.

In this case, we just recursively return `give_change(T, [C_1, C_2, ..., C_n])`.

We return whichever of Case 1 and Case 2 has the smaller number of coins.
